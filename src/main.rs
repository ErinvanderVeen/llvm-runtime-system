use once_cell::unsync::Lazy;

//static A: Lazy<Mutex<Vec<i64>>> = Lazy::new(|| Mutex::new(Vec::with_capacity(1024)));
static mut B: Lazy<Vec<i64>> = Lazy::new(|| Vec::with_capacity(1024));
//static C: Lazy<Mutex<Vec<i64>>> = Lazy::new(|| Mutex::new(Vec::with_capacity(1024)));

extern {
    fn start();
}

#[no_mangle]
pub unsafe fn peek_b_b() -> bool {
    //println!("peek_b_b B: {:?}", b);
    let top = *B.last().expect("RTS.peek_b_b: Tried to peek empty B stack");
    B.pop();
    top != 0
}

#[no_mangle]
pub unsafe fn eqI_b(element: i64, offset: i64) {
    //println!("eqI_b B: {:?}", b);
    let target = B.len().saturating_sub(offset as usize + 1);
    if B[target] == element {
        B.push(1)
    } else {
        B.push(0)
    }
}

#[no_mangle]
pub unsafe fn pop_b(offset: i64) {
    //println!("pop_b B: {:?}", b);
    let target_length = B.len().saturating_sub(offset as usize);
    B.truncate(target_length);
}

#[no_mangle]
pub unsafe fn pushI(element: i64) {
    //println!("pushI B: {:?}", b);
    B.push(element);
}

#[no_mangle]
pub unsafe fn push_b(offset: i64) {
    //println!("push_b B: {:?}", b);
    let target = B.len().saturating_sub(offset as usize + 1);
    let element = B[target];
    B.push(element);
}

#[no_mangle]
pub unsafe fn subI() {
    //println!("subI B: {:?}", b);
    let op1 = B.pop().expect("RTS.subI: Tried to pop from empty B stack");
    let op2 = B.pop().expect("RTS.subI: Tried to pop from empty B stack");
    B.push(op1 - op2);
}

#[no_mangle]
pub unsafe fn addI() {
    //println!("addI B: {:?}", b);
    let op1 = B.pop().expect("RTS.addI: Tried to pop from empty B stack");
    let op2 = B.pop().expect("RTS.addI: Tried to pop from empty B stack");
    B.push(op1 + op2);
}

#[no_mangle]
pub unsafe fn update_b(offset_f: i64, offset_t: i64) {
    //println!("update_b B: {:?}", b);
    let target_f = B.len().saturating_sub(offset_f as usize + 1);
    let target_t = B.len().saturating_sub(offset_t as usize + 1);
    B[target_t] = B[target_f];
}

#[no_mangle]
pub unsafe fn updatepop_b(offset_f: i64, offset_t: i64) {
    //println!("updatepop_b B: {:?}", b);
    let target_f = B.len().saturating_sub(offset_f as usize + 1);
    let target_t = B.len().saturating_sub(offset_t as usize + 1);
    B[target_t] = B[target_f];
    B.truncate(target_t + 1);
}

fn main() {
    unsafe {
        start();
        let n = B.pop().unwrap();
        println!("Result: {}", n);
    }
}
